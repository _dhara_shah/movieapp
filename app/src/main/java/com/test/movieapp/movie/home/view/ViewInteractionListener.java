package com.test.movieapp.movie.home.view;

import android.view.View;

public interface ViewInteractionListener {
    void onMoreInfoClicked(View view, long movieId);

    void onLoadMore();

    void onDateSet(String date);
}
