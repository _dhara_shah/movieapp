package com.test.movieapp.movie.home.router;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;

import com.test.movieapp.movie.moviedetails.view.MovieDetailActivity;

public class MainRouterImpl implements MainRouter {
    private final AppCompatActivity activity;

    public MainRouterImpl(@NonNull final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void openDetails(final long movieId, final ActivityOptionsCompat options) {
        MovieDetailActivity.startActivity(activity, movieId, options);
    }
}
