package com.test.movieapp.movie.home.model;

import android.support.annotation.IntDef;
import android.view.View;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface MoviesModelAdapter {
    int VIEW_TYPE_ROW = 1;
    int VIEW_TYPE_LOAD_MORE = 2;

    int getCount();

    String getMovieTitle(int position);

    String getDesc(int position);

    String getImageUrl(int position);

    String getAverageVote(int position);

    String getReleaseDate(int position);

    long getMovieId(int position);

    int getItemViewType(int position);

    boolean isLoadMore();

    String getFilterDate();

    @Visibility
    int getImageVisibility(int position);

    @Visibility
    int getDescriptionVisibility(int position);

    boolean hasImage(int position);

    @IntDef({View.VISIBLE, View.INVISIBLE, View.GONE})
    @Retention(RetentionPolicy.SOURCE)
    @interface Visibility {

    }
}
