package com.test.movieapp.movie.home.view;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.test.movieapp.R;
import com.test.movieapp.movie.home.model.MoviesModelAdapter;
import com.test.movieapp.movie.view.MovieDateFragment;
import com.test.movieapp.utils.views.GridSpacingItemDecoration;

public class MainViewImpl implements MainView {
    private static final int MAX_SPAN_COUNT = 2;
    private boolean canLoadMore = true;
    private int visibleThreshold = 5;
    private final AppCompatActivity activity;
    private ViewInteractionListener listener;
    private MoviesAdapter moviesAdapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private int totalItemCount;
    private int lastVisibleItem;
    private boolean isLoading;

    public MainViewImpl(@NonNull final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(@NonNull final MoviesModelAdapter modelAdapter) {
        initToolbar();

        recyclerView = activity.findViewById(R.id.recycler_view_movies);
        progressBar = activity.findViewById(R.id.progress_bar);

        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(MAX_SPAN_COUNT,
                StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        final int marginSpace = activity.getApplicationContext().getResources().getDimensionPixelSize(R.dimen.padding_tiny);
        final GridSpacingItemDecoration itemDecoration = new GridSpacingItemDecoration(MAX_SPAN_COUNT, marginSpace, true);
        recyclerView.addItemDecoration(itemDecoration);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(final RecyclerView recyclerView, final int dx, final int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (canLoadMore) {
                    totalItemCount = layoutManager.getItemCount();
                    final int[] into = layoutManager.findFirstCompletelyVisibleItemPositions(null);

                    if (into != null) {
                        lastVisibleItem = into[0];
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            if (listener != null) {
                                listener.onLoadMore();
                                isLoading = true;
                            }
                        }
                    } else {
                        isLoading = false;
                    }
                }
            }
        });

        moviesAdapter = MoviesAdapter.createFrom(modelAdapter, listener);
        recyclerView.setAdapter(moviesAdapter);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void setInteractionListener(@NonNull final ViewInteractionListener listener) {
        this.listener = listener;
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void updateMovies(@NonNull final MoviesModelAdapter modelAdapter) {
        moviesAdapter = MoviesAdapter.createFrom(modelAdapter, listener);
        recyclerView.swapAdapter(moviesAdapter, false);
        isLoading = false;
    }

    @Override
    public void endLoadMore() {
        canLoadMore = false;
    }

    @Override
    public void showDatePicker() {
        final MovieDateFragment fragment = MovieDateFragment.newInstance();
        fragment.setListener(listener);
        fragment.show(activity.getSupportFragmentManager(), "date_picker");
    }

    @Override
    public void showError(final String errorMessage) {
        Snackbar.make(recyclerView, errorMessage, Snackbar.LENGTH_SHORT);
    }

    @Override
    public void toggleView(final ViewType viewType, @NonNull final MoviesModelAdapter modelAdapter) {
        switch (viewType) {
            case GRID:
                final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(MAX_SPAN_COUNT,
                        StaggeredGridLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setHasFixedSize(true);
                moviesAdapter = MoviesAdapter.createFrom(modelAdapter, listener);
                moviesAdapter.setViewType(viewType);
                recyclerView.setAdapter(moviesAdapter);
                isLoading = false;
                break;

            case LIST:
                recyclerView.removeAllViews();
                final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setHasFixedSize(true);

                moviesAdapter = MoviesAdapter.createFrom(modelAdapter, listener);
                moviesAdapter.setViewType(viewType);
                recyclerView.setAdapter(moviesAdapter);
                isLoading = false;
                break;
        }
    }

    @Override
    public ActivityOptionsCompat getActivityOptions(final View view) {
        return ActivityOptionsCompat.
                makeSceneTransitionAnimation(activity, view,
                        view.getContext().getString(R.string.transition_name));
    }

    private void initToolbar() {
        final Toolbar toolbar = activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);

        toolbar.setAlpha(0);
        toolbar.setTranslationY(-300);

        toolbar.animate().setDuration(1000).translationY(0).alpha(1);

        for(int i = 0; i < toolbar.getChildCount(); i++ ){
            final View view = toolbar.getChildAt(i);
            view.setTranslationY(-300);
            view.animate().setStartDelay(900).setDuration(1000).translationY(0);
        }
    }
}
