package com.test.movieapp.movie.home.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikepenz.iconics.view.IconicsTextView;

import com.test.movieapp.R;
import com.test.movieapp.movie.home.model.MoviesModelAdapter;
import com.test.movieapp.movie.home.view.MainView.ViewType;

public final class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private MoviesModelAdapter modelAdapter;
    private ViewInteractionListener listener;
    private ViewType layoutViewType;

    static MoviesAdapter createFrom(@NonNull final MoviesModelAdapter modelAdapter, @NonNull ViewInteractionListener listener) {
        return new MoviesAdapter(modelAdapter, listener);
    }

    private MoviesAdapter(@NonNull final MoviesModelAdapter modelAdapter, @NonNull final ViewInteractionListener listener) {
        this.modelAdapter = modelAdapter;
        this.listener = listener;
    }

    public void setViewType(final ViewType viewType) {
        layoutViewType = viewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        if (viewType == MoviesModelAdapter.VIEW_TYPE_LOAD_MORE) {
            final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_progress_bar, parent, false);
            return new ProgressViewHolder(itemView);
        } else {
            int layoutId = R.layout.layout_movie_item_grid;
            if (layoutViewType == ViewType.LIST) {
                layoutId = R.layout.layout_movie_item;
            }

            final View itemView = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
            return new ItemViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder vh, final int position) {
        final int viewType = vh.getItemViewType();

        if (viewType == MoviesModelAdapter.VIEW_TYPE_LOAD_MORE) {
            final ProgressViewHolder holder = (ProgressViewHolder) vh;
            holder.rootView.setVisibility(modelAdapter.isLoadMore() ? View.VISIBLE : View.GONE);
            holder.progressView.setVisibility(modelAdapter.isLoadMore() ? View.VISIBLE : View.GONE);
        } else {
            final ItemViewHolder holder = (ItemViewHolder)vh;
            holder.txtDesc.setText(modelAdapter.getDesc(position));
            holder.txtTitle.setText(modelAdapter.getMovieTitle(position));
            holder.iconFontStars.setText(holder.itemView.getContext().getString(R.string.icon_font_star,
                    modelAdapter.getAverageVote(position)));

            holder.iconReleaseDate.setText(String.format(holder.itemView.getContext().getString(R.string.icon_font_release_date),
                    modelAdapter.getReleaseDate(position)));

            holder.imgMovie.setVisibility(modelAdapter.getImageVisibility(position));
            holder.txtDesc.setVisibility(modelAdapter.getDescriptionVisibility(position));

            if (modelAdapter.hasImage(position)) {
                Glide.with(holder.itemView.getContext()).load(modelAdapter.getImageUrl(position)).into(holder.imgMovie);
            }
        }
    }

    @Override
    public int getItemCount() {
        return modelAdapter.getCount();
    }

    @Override
    public int getItemViewType(final int position) {
        return modelAdapter.getItemViewType(position);
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView txtDesc;
        TextView txtTitle;
        ImageView imgMovie;
        IconicsTextView iconFontStars;
        IconicsTextView iconReleaseDate;
        TextView txtMoreInfo;

        ItemViewHolder(final View itemView) {
            super(itemView);
            txtDesc = itemView.findViewById(R.id.txt_movie_description);
            txtTitle = itemView.findViewById(R.id.txt_movie_title);
            imgMovie = itemView.findViewById(R.id.img_movie);
            iconFontStars = itemView.findViewById(R.id.icon_font_star);
            iconReleaseDate = itemView.findViewById(R.id.icon_font_release_date);
            txtMoreInfo = itemView.findViewById(R.id.txt_more_info);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final int adapterPosition = getAdapterPosition();
                    if (adapterPosition == RecyclerView.NO_POSITION) {
                        return;
                    }

                    if (listener != null) {
                        listener.onMoreInfoClicked(itemView, modelAdapter.getMovieId(adapterPosition));
                    }
                }
            });
        }
    }

    static class ProgressViewHolder extends RecyclerView.ViewHolder {
        View rootView;
        View progressView;

        ProgressViewHolder(final View itemView) {
            super(itemView);
            rootView = itemView.findViewById(R.id.root_view);
            progressView  = itemView.findViewById(R.id.progress_bar);
        }
    }
}
