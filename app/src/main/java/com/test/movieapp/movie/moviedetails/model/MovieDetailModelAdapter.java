package com.test.movieapp.movie.moviedetails.model;

import android.support.annotation.IntDef;
import android.view.View;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface MovieDetailModelAdapter {
    String getTitle();

    String getOverview();

    String getImageUrl();

    int getGenreSize();

    String getGenre(int position);

    boolean hasGenres();

    String getTagline();

    float getAverageCount();

    boolean hasTagLine();

    String getReleaseDate();

    String getStatus();

    @Visibility
    int getTagLineVisibility();

    @Visibility
    int getGenresVisibility();

    @IntDef({View.VISIBLE, View.INVISIBLE, View.GONE})
    @Retention(RetentionPolicy.SOURCE)
    @interface Visibility {

    }
}
