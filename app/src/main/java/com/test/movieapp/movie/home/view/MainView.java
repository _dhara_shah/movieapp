package com.test.movieapp.movie.home.view;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;

import com.test.movieapp.movie.home.model.MoviesModelAdapter;

public interface MainView {
    void initViews(@NonNull MoviesModelAdapter modelAdapter);

    void showProgress();

    void setInteractionListener(@NonNull ViewInteractionListener listener);

    void hideProgress();

    void updateMovies(@NonNull MoviesModelAdapter modelAdapter);

    void endLoadMore();

    void showDatePicker();

    void showError(String errorMessage);

    void toggleView(ViewType viewType, @NonNull MoviesModelAdapter modelAdapter);

    ActivityOptionsCompat getActivityOptions(View view);

    enum ViewType {
        GRID(1), LIST(2);
        private final int value;

        ViewType(final int value) {
            this.value = value;
        }

        int getValue() {
            return value;
        }
    }
}
