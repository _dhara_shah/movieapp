package com.test.movieapp.movie.datasource;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.test.movieapp.network.Listener;
import com.test.movieapp.network.entity.MovieDetailResponse;
import com.test.movieapp.network.entity.MovieResponse;

public interface MainDataSource {
    void getMovies(@NonNull String discoverType,
                   @NonNull String apiKey,
                   int page,
                   @Nullable String sortBy,
                   @NonNull String releaseDate,
                   @NonNull Listener<MovieResponse> listener);

    void getMovieDetail(long movieId, final String apiKey, @NonNull Listener<MovieDetailResponse> listener);
}
