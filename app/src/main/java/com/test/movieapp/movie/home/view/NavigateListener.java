package com.test.movieapp.movie.home.view;

public interface NavigateListener {
    void onNavigateUp();
}
