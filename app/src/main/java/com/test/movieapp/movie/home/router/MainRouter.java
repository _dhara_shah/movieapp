package com.test.movieapp.movie.home.router;

import android.support.v4.app.ActivityOptionsCompat;

public interface MainRouter {
    void openDetails(long movieId, ActivityOptionsCompat options);
}
