package com.test.movieapp.movie.home.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.test.movieapp.MovieApp;
import com.test.movieapp.di.MovieAppInjector;
import com.test.movieapp.movie.datasource.MainDataSource;
import com.test.movieapp.network.Listener;
import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.api.Api;
import com.test.movieapp.network.entity.MovieResponse;
import com.test.movieapp.network.entity.Movies;
import com.test.movieapp.utils.DateFormatter;

public class MainInteractorImpl implements MainInteractor {
    private final static String discoverType = "movie";
    private final String apiKey = Api.getApiKey();
    private final static String sortBy = "release_date.desc";
    private MovieResponse movieResponse;
    private List<Movies> moviesList = new ArrayList<>();
    private String filterDate = DateFormatter.getCurrentDate();
    private int page = 1;

    @Inject
    MainDataSource dataSource;

    public MainInteractorImpl() {
        MovieAppInjector.from(MovieApp.INSTANCE).inject(this);
    }

    @Override
    public void fetchMovies(@NonNull final ResponseListener listener, final boolean firstTime) {
        if (firstTime) {
            page = 1;
        }

        dataSource.getMovies(discoverType, apiKey, page, sortBy, filterDate, new Listener<MovieResponse>() {
            @Override
            public void onSuccess(final MovieResponse response) {
                movieResponse = response;

                if (firstTime) {
                    moviesList.clear();
                }

                moviesList.addAll(response.getMovieList());

                if (isLoadMore()) {
                    page += 1;
                }

                listener.onResponseSuccess(response);
            }

            @Override
            public void onError(final ServiceError error) {
                listener.onResponseError(error);
            }

            @Override
            public void onComplete() {
                page = movieResponse.getTotalPages();
            }
        });
    }

    @Override
    public boolean isLoadMore() {
        return movieResponse != null && movieResponse.getTotalPages() > page;
    }

    @Override
    public void setFilterDate(final String filterDate) {
        this.filterDate = filterDate;
    }

    @Override
    public String getFilterDate() {
        return filterDate;
    }

    @Nullable
    @Override
    public List<Movies> getMovieList() {
        return moviesList;
    }
}
