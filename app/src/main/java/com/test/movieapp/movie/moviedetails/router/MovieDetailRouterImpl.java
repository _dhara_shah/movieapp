package com.test.movieapp.movie.moviedetails.router;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

public class MovieDetailRouterImpl implements MovieDetailRouter {
    private final AppCompatActivity activity;

    public MovieDetailRouterImpl(@NonNull final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void close() {
        activity.supportFinishAfterTransition();
    }
}
