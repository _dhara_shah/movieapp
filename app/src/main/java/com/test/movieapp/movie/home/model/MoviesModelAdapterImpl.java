package com.test.movieapp.movie.home.model;

import android.text.TextUtils;
import android.view.View;

import java.text.DecimalFormat;
import java.util.List;

import com.test.movieapp.network.entity.Movies;
import com.test.movieapp.utils.DateFormatter;

public class MoviesModelAdapterImpl implements MoviesModelAdapter {
    private List<Movies> movieList;
    private boolean loadMore;
    private String filterDate;

    public MoviesModelAdapterImpl(final List<Movies> movieList, final boolean loadMore, final String filterDate) {
        this.movieList = movieList;
        this.loadMore = loadMore;
        this.filterDate = filterDate;
    }

    @Override
    public int getCount() {
        return isEmpty() ? 0 : movieList.size() + 1;
    }

    @Override
    public String getMovieTitle(final int position) {
        return isEmpty() ? "" : movieList.get(position).getTitle();
    }

    @Override
    public String getDesc(final int position) {
        return isEmpty() ? "" : movieList.get(position).getOverview();
    }

    @Override
    public String getImageUrl(final int position) {
        return isEmpty() ? "" : movieList.get(position).getPosterPath();
    }

    @Override
    public String getAverageVote(final int position) {
        final float averageVote = isEmpty() ? 0L :  movieList.get(position).getVoteAverage();
        final DecimalFormat df = new DecimalFormat("#.##");
        return df.format(averageVote);
    }

    @Override
    public String getReleaseDate(final int position) {
        return isEmpty() ? "" : DateFormatter.getReleaseDate(movieList.get(position).getReleaseDate());
    }

    @Override
    public long getMovieId(final int position) {
        return isEmpty() ? 0 : movieList.get(position).getMovieId();
    }

    @Override
    public int getItemViewType(final int position) {
        if (position >= movieList.size()) {
            return VIEW_TYPE_LOAD_MORE;
        }
        return VIEW_TYPE_ROW;
    }

    @Override
    public boolean isLoadMore() {
        return loadMore;
    }

    @Override
    public String getFilterDate() {
        return filterDate;
    }

    @Visibility
    @Override
    public int getImageVisibility(final int position) {
        return TextUtils.isEmpty(movieList.get(position).getPosterPath()) ? View.GONE : View.VISIBLE;
    }

    @Override
    public int getDescriptionVisibility(final int position) {
        return TextUtils.isEmpty(getDesc(position)) ? View.GONE : View.VISIBLE;
    }

    @Override
    public boolean hasImage(final int position) {
        return !TextUtils.isEmpty(getImageUrl(position));
    }

    private boolean isEmpty() {
        return movieList == null || movieList.isEmpty();
    }
}
