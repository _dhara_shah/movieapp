package com.test.movieapp.movie.moviedetails.view;

import android.support.annotation.NonNull;

import com.test.movieapp.movie.home.view.NavigateListener;
import com.test.movieapp.movie.moviedetails.model.MovieDetailModelAdapter;

public interface MovieDetailView {
    void initViews(@NonNull NavigateListener listener);

    void showProgress();

    void hideProgress();

    void updateData(@NonNull MovieDetailModelAdapter modelAdapter);
}
