package com.test.movieapp.movie.home.presenter;

import com.test.movieapp.movie.home.view.MainView;

public interface MainPresenter {
    void handleOnViewCreated();

    void handleOnFilterClicked();

    void handleToggleView(MainView.ViewType viewType);

    MainPresenter STUB = new MainPresenter() {
        @Override
        public void handleOnViewCreated() {

        }

        @Override
        public void handleOnFilterClicked() {

        }

        @Override
        public void handleToggleView(final MainView.ViewType viewType) {

        }
    };
}
