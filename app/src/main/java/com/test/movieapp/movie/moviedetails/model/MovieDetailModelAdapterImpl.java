package com.test.movieapp.movie.moviedetails.model;

import android.text.TextUtils;
import android.view.View;

import com.test.movieapp.network.api.Api;
import com.test.movieapp.network.entity.MovieDetailResponse;
import com.test.movieapp.utils.DateFormatter;

public class MovieDetailModelAdapterImpl implements MovieDetailModelAdapter {
    private MovieDetailResponse response;

    public MovieDetailModelAdapterImpl(MovieDetailResponse response) {
        this.response = response;
    }

    @Override
    public String getTitle() {
        return response.getTitle();
    }

    @Override
    public String getOverview() {
        return response.getOverview();
    }

    @Override
    public String getImageUrl() {
        final String backdropPath = response.getBackdropPath();
        final String posterPath = response.getPosterPath();

        return TextUtils.isEmpty(posterPath) ?
                (TextUtils.isEmpty(backdropPath) ? "" : Api.getImageBaseUrl() + backdropPath) :
                Api.getImageBaseUrl() + posterPath;
    }

    @Override
    public int getGenreSize() {
        return isGenresEmpty() ? 0 : response.getGenres().size();
    }

    @Override
    public String getGenre(final int position) {
        return isGenresEmpty() ? "" : response.getGenres().get(position).getGenre();
    }

    @Override
    public boolean hasGenres() {
        return !isGenresEmpty();
    }

    @Override
    public String getTagline() {
        return TextUtils.isEmpty(response.getTagline()) ? "" : response.getTagline();
    }

    @Override
    public boolean hasTagLine() {
        return !TextUtils.isEmpty(response.getTagline());
    }

    @Override
    public String getReleaseDate() {
        return TextUtils.isEmpty(response.getReleaseDate()) ? "" : DateFormatter.getFullReleaseDate(response.getReleaseDate());
    }

    @Override
    public String getStatus() {
        return TextUtils.isEmpty(response.getStatus()) ? "" : response.getStatus();
    }

    @Visibility
    @Override
    public int getTagLineVisibility() {
        return hasTagLine() ? View.VISIBLE : View.GONE;
    }

    @Override
    public float getAverageCount() {
        return response.getAverageVote() / 2;
    }

    @Visibility
    @Override
    public int getGenresVisibility() {
        return isGenresEmpty() ? View.GONE : View.VISIBLE;
    }

    private boolean isGenresEmpty() {
        return response.getGenres() == null || response.getGenres().isEmpty();
    }
}
