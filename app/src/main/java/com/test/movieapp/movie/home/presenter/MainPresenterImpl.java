package com.test.movieapp.movie.home.presenter;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;

import javax.inject.Inject;

import com.test.movieapp.MovieApp;
import com.test.movieapp.di.MovieAppInjector;
import com.test.movieapp.movie.home.model.MainInteractor;
import com.test.movieapp.movie.home.model.MoviesModelAdapterImpl;
import com.test.movieapp.movie.home.router.MainRouter;
import com.test.movieapp.movie.home.view.MainView;
import com.test.movieapp.movie.home.view.ViewInteractionListener;
import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.entity.MovieResponse;

public class MainPresenterImpl implements MainPresenter, MainInteractor.ResponseListener, ViewInteractionListener {
    private MainView mainView;
    private MainRouter router;

    @Inject
    MainInteractor mainInteractor;

    public MainPresenterImpl(@NonNull final MainView view, @NonNull final MainRouter router) {
        this.mainView = view;
        this.router = router;
        MovieAppInjector.from(MovieApp.INSTANCE).inject(this);
    }

    @Override
    public void handleOnViewCreated() {
        mainView.setInteractionListener(this);
        mainView.initViews(new MoviesModelAdapterImpl(mainInteractor.getMovieList(),
                mainInteractor.isLoadMore(),
                mainInteractor.getFilterDate()));
        mainView.showProgress();
        mainInteractor.fetchMovies(this, true);
    }

    @Override
    public void handleOnFilterClicked() {
        mainView.showDatePicker();
    }

    @Override
    public void handleToggleView(final MainView.ViewType viewType) {
        mainView.toggleView(viewType, new MoviesModelAdapterImpl(mainInteractor.getMovieList(),
                mainInteractor.isLoadMore(),
                mainInteractor.getFilterDate()));
    }

    @Override
    public void onResponseSuccess(final MovieResponse movieResponse) {
        mainView.hideProgress();
        mainView.updateMovies(new MoviesModelAdapterImpl(mainInteractor.getMovieList(),
                mainInteractor.isLoadMore(),
                mainInteractor.getFilterDate()));
    }

    @Override
    public void onResponseError(final ServiceError error) {
        mainView.hideProgress();
        mainView.showError(error.getErrorMessage());
    }

    @Override
    public void onMoreInfoClicked(final View view, final long movieId) {
        ActivityOptionsCompat options = mainView.getActivityOptions(view);
        router.openDetails(movieId, options);
    }

    @Override
    public void onLoadMore() {
        if (mainInteractor.isLoadMore()) {
            mainInteractor.fetchMovies(this, false);
            return;
        }
        mainView.endLoadMore();
    }

    @Override
    public void onDateSet(final String date) {
        mainInteractor.setFilterDate(date);
        mainView.showProgress();
        mainInteractor.fetchMovies(this, true);
    }
}
