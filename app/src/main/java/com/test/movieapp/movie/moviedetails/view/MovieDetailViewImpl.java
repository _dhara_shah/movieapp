package com.test.movieapp.movie.moviedetails.view;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.flexbox.FlexboxLayout;
import com.test.movieapp.R;
import com.test.movieapp.movie.home.view.NavigateListener;
import com.test.movieapp.movie.moviedetails.model.MovieDetailModelAdapter;

public class MovieDetailViewImpl implements MovieDetailView {
    private AppCompatActivity activity;
    private ProgressBar progressBar;
    private TextView txtOverview;
    private ImageView imgPoster;
    private TextView txtMovieTitle;
    private FlexboxLayout flexboxLayout;
    private TextView lblGenre;
    private AppCompatRatingBar ratingBar;
    private TextView txtTagLine;
    private TextView txtReleaseDate;
    private TextView txtStatus;

    public MovieDetailViewImpl(@NonNull final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(@NonNull NavigateListener listener) {
        setUpActionBar(listener);
        findViews();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void updateData(@NonNull final MovieDetailModelAdapter modelAdapter) {
        txtOverview.setText(modelAdapter.getOverview());

        txtMovieTitle.setText(modelAdapter.getTitle());

        Glide.with(activity)
                .load(modelAdapter.getImageUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable final GlideException e,
                                                final Object model,
                                                final Target<Drawable> target,
                                                final boolean isFirstResource) {
                        activity.supportStartPostponedEnterTransition();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final Drawable resource,
                                                   final Object model,
                                                   final Target<Drawable> target,
                                                   final DataSource dataSource,
                                                   final boolean isFirstResource) {
                        activity.supportStartPostponedEnterTransition();
                        return false;
                    }
                })
                .apply(new RequestOptions().placeholder(R.drawable.no_image))
                .into(imgPoster);


        lblGenre.setVisibility(modelAdapter.getGenresVisibility());
        flexboxLayout.setVisibility(modelAdapter.getGenresVisibility());

        if (modelAdapter.hasGenres()) {
            flexboxLayout.removeAllViews();
            for (int position = 0; position < modelAdapter.getGenreSize(); position++) {
                final View view = LayoutInflater.from(activity).inflate(R.layout.layout_genre, flexboxLayout, false);
                final TextView txtGenre = view.findViewById(R.id.txt_genre);
                txtGenre.setText(modelAdapter.getGenre(position));
                flexboxLayout.addView(view);
            }
        }

        ratingBar.setRating(modelAdapter.getAverageCount());
        txtTagLine.setText(modelAdapter.getTagline());
        txtTagLine.setVisibility(modelAdapter.getTagLineVisibility());
        txtStatus.setText(modelAdapter.getStatus());

        txtReleaseDate.setText(activity.getString(R.string.icon_font_release_date, modelAdapter.getReleaseDate()));
    }

    private void setUpActionBar(@NonNull final NavigateListener listener) {
        final Toolbar toolbar = activity.findViewById(R.id.toolbar);
        txtMovieTitle = activity.findViewById(R.id.txt_movie_title);
        activity.setSupportActionBar(toolbar);

        final ActionBar actionBar = this.activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(android.R.drawable.ic_menu_close_clear_cancel);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                listener.onNavigateUp();
            }
        });
    }

    private void findViews() {
        progressBar = activity.findViewById(R.id.progress_bar);
        txtOverview = activity.findViewById(R.id.txt_movie_overview);
        imgPoster = activity.findViewById(R.id.img_poster);
        flexboxLayout = activity.findViewById(R.id.flex_genres);
        lblGenre = activity.findViewById(R.id.txt_genre_label);
        ratingBar = activity.findViewById(R.id.rating_bar);
        txtTagLine = activity.findViewById(R.id.txt_tagline);
        txtReleaseDate = activity.findViewById(R.id.txt_release_date);
        txtStatus = activity.findViewById(R.id.txt_status);
    }
}
