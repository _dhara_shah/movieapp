package com.test.movieapp.movie.datasource;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import com.test.movieapp.MovieApp;
import com.test.movieapp.di.MovieAppInjector;
import com.test.movieapp.network.Listener;
import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.api.RestApi;
import com.test.movieapp.network.entity.MovieDetailResponse;
import com.test.movieapp.network.entity.MovieResponse;
import com.test.movieapp.utils.MovieAppLog;

public class MainDataSourceImpl implements MainDataSource {
    private static final String TAG = MainDataSourceImpl.class.getSimpleName();
    @Inject
    Retrofit retrofit;

    public MainDataSourceImpl() {
        MovieAppInjector.from(MovieApp.INSTANCE).inject(this);
    }

    @Override
    public void getMovies(@NonNull final String discoverType,
                          @NonNull final String apiKey,
                          final int page,
                          @Nullable final String sortBy,
                          @NonNull final String releaseDate,
                          @NonNull final Listener<MovieResponse> listener) {

        final Call<MovieResponse> callable = retrofit.create(RestApi.class)
                .getMovies(discoverType, apiKey, page, sortBy, releaseDate);
        callable.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(final Call<MovieResponse> call, final Response<MovieResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(final Call<MovieResponse> call, final Throwable t) {
                MovieAppLog.wtf(TAG, t);
                listener.onError(new ServiceError(t.getMessage()));
            }
        });
    }

    @Override
    public void getMovieDetail(final long movieId, final String apiKey, @NonNull final Listener<MovieDetailResponse> listener) {
        final Call<MovieDetailResponse> call = retrofit.create(RestApi.class).getMovieDetails(movieId, apiKey);
        call.enqueue(new Callback<MovieDetailResponse>() {
            @Override
            public void onResponse(final Call<MovieDetailResponse> call, final Response<MovieDetailResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(final Call<MovieDetailResponse> call, final Throwable t) {
                listener.onError(new ServiceError(t.getMessage()));
            }
        });
    }
}
