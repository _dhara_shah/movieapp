package com.test.movieapp.movie.moviedetails.model;

import javax.inject.Inject;

import com.test.movieapp.MovieApp;
import com.test.movieapp.di.MovieAppInjector;
import com.test.movieapp.movie.datasource.MainDataSource;
import com.test.movieapp.network.Listener;
import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.api.Api;
import com.test.movieapp.network.entity.MovieDetailResponse;

public class MovieDetailInteractorImpl implements MovieDetailInteractor {
    private long movieId;

    @Inject
    MainDataSource dataSource;

    public MovieDetailInteractorImpl(final long movieId) {
        this.movieId = movieId;
        MovieAppInjector.from(MovieApp.INSTANCE).inject(this);
    }

    @Override
    public void fetchDetail(final DetailResponseListener listener) {
        dataSource.getMovieDetail(movieId, Api.getApiKey(), new Listener<MovieDetailResponse>() {
            @Override
            public void onSuccess(final MovieDetailResponse response) {
                listener.onResponseSuccess(response);
            }

            @Override
            public void onError(final ServiceError error) {
                listener.onResponseError(error);
            }

            @Override
            public void onComplete() {

            }
        });
    }
}
