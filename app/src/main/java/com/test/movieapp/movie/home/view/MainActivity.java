package com.test.movieapp.movie.home.view;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.test.movieapp.R;
import com.test.movieapp.movie.home.presenter.MainPresenter;
import com.test.movieapp.movie.home.presenter.MainPresenterImpl;
import com.test.movieapp.movie.home.router.MainRouter;
import com.test.movieapp.movie.home.router.MainRouterImpl;

public class MainActivity extends AppCompatActivity {
    MainPresenter presenter = MainPresenter.STUB;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final MainRouter router = new MainRouterImpl(this);
        final MainView view = new MainViewImpl(this);

        presenter = new MainPresenterImpl(view, router);
        presenter.handleOnViewCreated();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        final MenuItem item = menu.findItem(R.id.action_toggle_view);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_list_view, null);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, Color.WHITE);
        item.setIcon(drawable);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == R.id.action_date_filter) {
            presenter.handleOnFilterClicked();
            return true;
        } else if (item.getItemId() == R.id.action_toggle_view) {
            final MainView.ViewType viewType;
            if (item.getTitle().equals(getString(R.string.action_list_view))) {
                Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_grid_view, null);
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable, Color.WHITE);

                item.setIcon(drawable);
                item.setTitle(getString(R.string.action_grid_view));
                viewType = MainView.ViewType.LIST;
            } else {
                Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_list_view, null);
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable, Color.WHITE);

                item.setIcon(drawable);
                item.setTitle(getString(R.string.action_list_view));
                viewType = MainView.ViewType.GRID;
            }
            presenter.handleToggleView(viewType);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
