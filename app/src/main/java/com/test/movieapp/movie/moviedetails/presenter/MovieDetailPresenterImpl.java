package com.test.movieapp.movie.moviedetails.presenter;

import android.support.annotation.NonNull;

import com.test.movieapp.movie.home.view.NavigateListener;
import com.test.movieapp.movie.moviedetails.model.MovieDetailInteractor;
import com.test.movieapp.movie.moviedetails.model.MovieDetailModelAdapterImpl;
import com.test.movieapp.movie.moviedetails.router.MovieDetailRouter;
import com.test.movieapp.movie.moviedetails.view.MovieDetailView;
import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.entity.MovieDetailResponse;

public class MovieDetailPresenterImpl implements MovieDetailPresenter, MovieDetailInteractor.DetailResponseListener, NavigateListener {
    private final MovieDetailView view;
    private final MovieDetailRouter router;
    private final MovieDetailInteractor interactor;

    public MovieDetailPresenterImpl(@NonNull final MovieDetailView view,
                                    @NonNull final MovieDetailInteractor interactor,
                                    @NonNull final MovieDetailRouter router) {
        this.view = view;
        this.router = router;
        this.interactor = interactor;
    }

    @Override
    public void onViewCreated() {
        view.initViews(this);
        view.showProgress();
        interactor.fetchDetail(this);
    }

    @Override
    public boolean handleOnBackPress() {
        router.close();
        return true;
    }

    @Override
    public void onResponseSuccess(final MovieDetailResponse response) {
        view.hideProgress();
        view.updateData(new MovieDetailModelAdapterImpl(response));
    }

    @Override
    public void onResponseError(final ServiceError error) {
        view.hideProgress();
    }

    @Override
    public void onNavigateUp() {
        handleOnBackPress();
    }
}
