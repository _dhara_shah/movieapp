package com.test.movieapp.movie.moviedetails.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;

import com.test.movieapp.R;
import com.test.movieapp.movie.moviedetails.model.MovieDetailInteractor;
import com.test.movieapp.movie.moviedetails.model.MovieDetailInteractorImpl;
import com.test.movieapp.movie.moviedetails.presenter.MovieDetailPresenter;
import com.test.movieapp.movie.moviedetails.presenter.MovieDetailPresenterImpl;
import com.test.movieapp.movie.moviedetails.router.MovieDetailRouter;
import com.test.movieapp.movie.moviedetails.router.MovieDetailRouterImpl;
import com.test.movieapp.utils.ConstantIntentExtra;

public class MovieDetailActivity extends AppCompatActivity {
    private MovieDetailPresenter presenter = MovieDetailPresenter.STUB;

    public static void startActivity(@NonNull final AppCompatActivity activity,
                                     final long movieId,
                                     final ActivityOptionsCompat options) {
        final Intent intent = new Intent(activity, MovieDetailActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putLong(ConstantIntentExtra.EXTRA_MOVIE_ID, movieId);
        intent.putExtra(ConstantIntentExtra.EXTRA_BUNDLE_MOVIE, bundle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            activity.startActivity(intent, options.toBundle());
        } else {
            activity.startActivity(intent);
        }
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        long movieId = -1;
        if (getIntent().getExtras() != null) {
            final Bundle bundle = getIntent().getBundleExtra(ConstantIntentExtra.EXTRA_BUNDLE_MOVIE);
            movieId = bundle.getLong(ConstantIntentExtra.EXTRA_MOVIE_ID);
        }

        final MovieDetailView  view = new MovieDetailViewImpl(this);
        final MovieDetailRouter router = new MovieDetailRouterImpl(this);
        final MovieDetailInteractor interactor = new MovieDetailInteractorImpl(movieId);
        presenter = new MovieDetailPresenterImpl(view, interactor, router);
        presenter.onViewCreated();
    }

    @Override
    public boolean onSupportNavigateUp() {
        presenter.handleOnBackPress();
        return true;
    }

    @Override
    public void onBackPressed() {
        presenter.handleOnBackPress();
    }
}
