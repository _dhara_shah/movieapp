package com.test.movieapp.dagger2.components;

import dagger.Component;
import com.test.movieapp.dagger2.modules.MovieAppModule;
import com.test.movieapp.di.ModuleScope;
import com.test.movieapp.di.components.CoreComponent;
import com.test.movieapp.movie.datasource.MainDataSourceImpl;
import com.test.movieapp.movie.home.model.MainInteractorImpl;
import com.test.movieapp.movie.moviedetails.model.MovieDetailInteractorImpl;
import com.test.movieapp.movie.home.presenter.MainPresenterImpl;

@ModuleScope
@Component(modules = MovieAppModule.class, dependencies = CoreComponent.class)
public interface MovieComponent {
    // inject the interactors, presenters if needed

    void inject(MainInteractorImpl mainInteractor);

    void inject(MainPresenterImpl mainPresenter);

    void inject(MainDataSourceImpl mainDataSource);

    void inject(MovieDetailInteractorImpl movieDetailInteractor);
}
