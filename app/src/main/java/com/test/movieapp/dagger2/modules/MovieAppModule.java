package com.test.movieapp.dagger2.modules;

import dagger.Module;
import dagger.Provides;
import com.test.movieapp.movie.datasource.MainDataSource;
import com.test.movieapp.movie.datasource.MainDataSourceImpl;
import com.test.movieapp.movie.home.model.MainInteractor;
import com.test.movieapp.movie.home.model.MainInteractorImpl;

@Module
public class MovieAppModule {
    // contains provides methods
    @Provides
    public MainInteractor provideMainInteractor() {
        return new MainInteractorImpl();
    }

    @Provides
    public MainDataSource provideMainDataSource() {
        return new MainDataSourceImpl();
    }
}
