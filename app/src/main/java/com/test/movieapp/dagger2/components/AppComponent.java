package com.test.movieapp.dagger2.components;

import dagger.Component;
import com.test.movieapp.dagger2.modules.AppModule;
import com.test.movieapp.di.ModuleScope;
import com.test.movieapp.di.components.CoreComponent;

@ModuleScope
@Component(modules = AppModule.class, dependencies = CoreComponent.class)
public interface AppComponent {
}
