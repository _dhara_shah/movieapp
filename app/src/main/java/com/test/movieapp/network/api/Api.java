package com.test.movieapp.network.api;

public class Api {
    private static final String BASE_URL = "https://api.themoviedb.org/3/";
    private static final String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w185";
    private static final String API_KEY = "11295dabbcbff76ce24be5bde5e69994";

    public static String getHost() {
        return BASE_URL;
    }

    public static String getImageBaseUrl() {
        return IMAGE_BASE_URL;
    }

    public static String getApiKey() {
        return API_KEY;
    }
}
