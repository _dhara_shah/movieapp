package com.test.movieapp.network.entity;

import com.google.gson.annotations.SerializedName;

public class Genres {
    private long id;

    @SerializedName("name")
    private String genre;

    public String getGenre() {
        return genre;
    }
}
