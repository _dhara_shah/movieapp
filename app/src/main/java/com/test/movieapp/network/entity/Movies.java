package com.test.movieapp.network.entity;

import com.google.gson.annotations.SerializedName;

import com.test.movieapp.network.api.Api;

public class Movies {
    @SerializedName("popularized")
    private
    long popularity;
    @SerializedName("vote_count")
    private
    int voteCount;
    @SerializedName("video")
    private
    boolean video;
    @SerializedName("poster_path")
    private
    String posterPath;
    @SerializedName("id")
    private
    long movieId;
    @SerializedName("adult")
    private
    boolean adult;
    @SerializedName("backdrop_path")
    private
    String backdropPath;
    @SerializedName("title")
    private
    String title;
    @SerializedName("vote_average")
    private
    float voteAverage;
    @SerializedName("overview")
    private
    String overview;
    @SerializedName("release_date")
    private
    String releaseDate;

    public long getPopularity() {
        return popularity;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public boolean isVideo() {
        return video;
    }

    public String getPosterPath() {
        if (posterPath == null || posterPath.length() <= 0) {
            return null;
        }
        return Api.getImageBaseUrl() + posterPath;
    }

    public long getMovieId() {
        return movieId;
    }

    public boolean isAdult() {
        return adult;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public String getTitle() {
        return title;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public String getOverview() {
        return overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }
}
