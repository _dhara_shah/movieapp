package com.test.movieapp.network.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieDetailResponse {
    @SerializedName("backdrop_path")
    private String backdropPath;
    @SerializedName("original_title")
    private String title;
    @SerializedName("overview")
    private String overview;
    @SerializedName("genres")
    private List<Genres> genres;
    @SerializedName("poster_path")
    private String posterPath;
    @SerializedName("tagline")
    private String tagline;
    @SerializedName("vote_average")
    private Float averageVote;
    @SerializedName("release_date")
    private String releaseDate;
    @SerializedName("status")
    private String status;

    public String getBackdropPath() {
        return backdropPath;
    }

    public String getOverview() {
        return overview;
    }

    public String getTitle() {
        return title;
    }

    public List<Genres> getGenres() {
        return genres;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public String getTagline() {
        return tagline;
    }

    public float getAverageVote() {
        return averageVote == null ? 0F : averageVote;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getStatus() {
        return status;
    }
}
