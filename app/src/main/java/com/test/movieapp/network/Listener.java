package com.test.movieapp.network;

public interface Listener<T> {
    void onSuccess(T response);

    void onError(ServiceError error);

    void onComplete();
}
