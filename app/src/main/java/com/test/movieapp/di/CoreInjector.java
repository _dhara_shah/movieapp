package com.test.movieapp.di;

import android.content.Context;

import com.test.movieapp.di.components.CoreComponent;
import com.test.movieapp.di.components.CoreComponentProvider;

public final class CoreInjector {
    private CoreInjector() { }

    public static CoreComponent from(final Context context) {
        final CoreComponentProvider provider = (CoreComponentProvider) context.getApplicationContext();
        return provider.getCoreComponent();
    }
}
