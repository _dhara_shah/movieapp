package com.test.movieapp.di.components;

public interface CoreComponentProvider {
    CoreComponent getCoreComponent();
}
