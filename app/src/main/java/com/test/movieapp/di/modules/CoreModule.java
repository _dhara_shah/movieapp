package com.test.movieapp.di.modules;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.test.movieapp.utils.MovieAppLog;

@Module
public class CoreModule {
    private final Context context;

    public CoreModule(final Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    public MovieAppLog provideMovieAppLog() {
        return new MovieAppLog();
    }

    @Singleton
    @Provides
    Application provideApplication() {
        return (Application) context;
    }
}
