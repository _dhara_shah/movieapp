package com.test.movieapp.di;

import android.content.Context;

import com.test.movieapp.dagger2.components.MovieComponent;

public interface MovieComponentProvider {
    MovieComponent getMovieComponent(Context context);
}
