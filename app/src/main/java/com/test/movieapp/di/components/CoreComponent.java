package com.test.movieapp.di.components;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import com.test.movieapp.di.modules.CoreModule;
import com.test.movieapp.di.modules.NetModule;
import com.test.movieapp.utils.MovieAppLog;

@Singleton
@Component(modules = {CoreModule.class, NetModule.class})
public interface CoreComponent {
    MovieAppLog getMovieAppLog();

    Retrofit getRetrofit();

    Gson getGson();

    OkHttpClient getOkHttpClient();

    void inject(MovieAppLog movieAppLog);
}
