package com.test.movieapp.utils;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {
    private static final String TAG = DateFormatter.class.getSimpleName();
    private static final String CURRENT_DATE_FORMAT = "yyyy-MM-dd";
    private static final String RELEASE_DATE_FORMAT = "yyyy/MM/dd";
    private static final String FULL_RELEASE_DATE_FORMAT = "dd MMMM yyyy";

    public static String getCurrentDate() {
        final SimpleDateFormat format = new SimpleDateFormat(CURRENT_DATE_FORMAT, Locale.ENGLISH);
        return format.format(new Date());
    }

    public static String getReleaseDate(@NonNull final String releaseDate) {
        final SimpleDateFormat format = new SimpleDateFormat(RELEASE_DATE_FORMAT, Locale.ENGLISH);
        final SimpleDateFormat currFormat = new SimpleDateFormat(CURRENT_DATE_FORMAT, Locale.ENGLISH);
        try {
            return format.format(currFormat.parse(releaseDate));
        } catch (final ParseException e) {
            MovieAppLog.d(TAG, e.getMessage());
        }
        return "";
    }

    public static String getFullReleaseDate(@NonNull final String releaseDate) {
        final SimpleDateFormat format = new SimpleDateFormat(FULL_RELEASE_DATE_FORMAT, Locale.ENGLISH);
        final SimpleDateFormat currFormat = new SimpleDateFormat(CURRENT_DATE_FORMAT, Locale.ENGLISH);
        try {
            return format.format(currFormat.parse(releaseDate));
        } catch (final ParseException e) {
            MovieAppLog.d(TAG, e.getMessage());
        }
        return "";
    }
}
