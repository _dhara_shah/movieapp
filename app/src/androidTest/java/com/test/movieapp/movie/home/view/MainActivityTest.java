package com.test.movieapp.movie.home.view;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;

import com.test.movieapp.R;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testDateFilterClick() throws InterruptedException {
        Thread.sleep(1000);

        onView(withId(R.id.action_date_filter)).perform(click());

        onView(withClassName(Matchers.equalTo(DatePicker.class.getName())))
                .perform(PickerActions.setDate(2017, 12, 28));

        onView(withId(android.R.id.button1)).perform(click());
    }

    @Test
    public void testOpenDetails() {
        onView(allOf(withId(R.id.recycler_view_movies), isDisplayed()))
                .perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.img_movie)));

        onView(withId(R.id.recycler_view_movies))
                .perform(actionOnItemAtPosition(0, click()));
    }

    @Test
    public void testOnNavigateUp() throws InterruptedException {
        onView(allOf(withId(R.id.recycler_view_movies), isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(10));

        onView(withId(R.id.recycler_view_movies))
                .perform(actionOnItemAtPosition(0, click()));

        onView(withId(R.id.txt_movie_overview)).check(matches(isDisplayed()));
        onView(withContentDescription("Navigate up")).perform(click());
        onView(withId(R.id.action_toggle_view)).check(matches(isDisplayed()));
        onView(withId(R.id.action_date_filter)).check(matches(isDisplayed()));
    }

    public static ViewAction clickChildViewWithId(final int id) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on a child view with specified id.";
            }

            @Override
            public void perform(final UiController uiController, final View view) {
                final View v = view.findViewById(id);
                v.performClick();
            }
        };
    }

}
