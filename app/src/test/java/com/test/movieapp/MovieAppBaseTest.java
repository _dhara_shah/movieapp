package com.test.movieapp;

import android.content.Context;
import android.support.annotation.CallSuper;

import com.test.movieapp.dagger2.components.DaggerMovieComponent;
import com.test.movieapp.dagger2.components.MovieComponent;
import com.test.movieapp.dagger2.modules.MovieAppModule;
import com.test.movieapp.di.MovieAppInjector;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MovieAppInjector.class)
public class MovieAppBaseTest extends CoreBaseTest {
    @Mock
    protected MovieAppModule movieAppModule;

    @Override
    @CallSuper
    public void setUp() throws Exception {
        super.setUp();

        final MovieComponent movieComponent = DaggerMovieComponent
                .builder()
                .coreComponent(coreComponent)
                .movieAppModule(movieAppModule)
                .build();
        PowerMockito.mockStatic(MovieAppInjector.class);
        PowerMockito.when(MovieAppInjector.from(any(Context.class))).thenReturn(movieComponent);
    }
}