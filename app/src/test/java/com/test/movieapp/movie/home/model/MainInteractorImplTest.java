package com.test.movieapp.movie.home.model;

import com.test.movieapp.MovieAppBaseTest;
import com.test.movieapp.movie.datasource.MainDataSource;
import com.test.movieapp.network.Listener;
import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.api.Api;
import com.test.movieapp.network.entity.MovieResponse;
import com.test.movieapp.network.entity.Movies;
import com.test.movieapp.utils.DateFormatter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@PrepareForTest({Api.class, DateFormatter.class})
@RunWith(PowerMockRunner.class)
public class MainInteractorImplTest extends MovieAppBaseTest {
    private final String sortBy = "release_date.desc";
    private final String discoverType = "movie";
    @Mock
    MainDataSource dataSource;
    @Mock
    MovieResponse response;
    @Mock
    ServiceError error;
    @Mock
    MainInteractor.ResponseListener listener;
    @Captor
    ArgumentCaptor<Listener<MovieResponse>> responseListener;

    private MainInteractorImpl interactor;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        PowerMockito.mockStatic(Api.class, DateFormatter.class);
        when(movieAppModule.provideMainDataSource()).thenReturn(dataSource);
        when(Api.getApiKey()).thenReturn("1234");
        when(DateFormatter.getCurrentDate()).thenReturn("2017-12-27");
        interactor = new MainInteractorImpl();
    }

    @Test
    public void testFetchMovieListSuccess() {
        final List<Movies> movieList = new ArrayList<>();

        when(response.getTotalPages()).thenReturn(10);
        when(response.getMovieList()).thenReturn(movieList);

        interactor.fetchMovies(listener, true);
        verify(dataSource).getMovies(eq(discoverType), eq("1234"),
                eq(1), eq(sortBy), eq("2017-12-27"), responseListener.capture());
        responseListener.getValue().onSuccess(response);

        assertTrue(response.getTotalPages() > 1);
        verify(response, atLeast(2)).getTotalPages();
        verify(response).getMovieList();
        verify(listener).onResponseSuccess(response);
    }

    @Test
    public void testFetchMovieListError() {
        interactor.fetchMovies(listener, true);
        verify(dataSource).getMovies(eq(discoverType), eq("1234"),
                eq(1), eq(sortBy), eq("2017-12-27"), responseListener.capture());
        responseListener.getValue().onError(error);
        verify(listener).onResponseError(error);
    }

    @Test
    public void testIsLoadMore() {
        when(response.getTotalPages()).thenReturn(10);
        interactor.isLoadMore();
        assertTrue(response != null && response.getTotalPages() > 1);
        verify(response).getTotalPages();
    }

    @Test
    public void testGetSetFilterDate() {
        interactor.setFilterDate("1234");
        assertTrue(interactor.getFilterDate().equals("1234"));
    }

    @After
    public void testAfter() {
        verifyNoMoreInteractions(response, listener, dataSource, error);
    }
}
