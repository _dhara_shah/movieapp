package com.test.movieapp.movie.home.model;

import com.test.movieapp.network.api.Api;
import com.test.movieapp.network.entity.Movies;
import com.test.movieapp.utils.DateFormatter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@PrepareForTest({Api.class, DateFormatter.class})
@RunWith(PowerMockRunner.class)
public class MoviesModelAdapterImplTest {
    private static final String MOVIE_TITLE = "title";
    private static final String MOVIE_OVERVIEW = "overview";
    private static final String MOVIE_IMAGE_URL = "imageurl";
    private static final String MOVIE_RELEASE_DATE = "2017-10-22";
    private static final String FORMATTED_DATE = "2017/10/22";
    private static final String IMAGE_BASE = "imagebase";
    private static final String FILTER_DATE = "2017-12-27";
    private static final float MOVIE_VOTE_AVG = 7.5f;
    private static final String STR_MOVIE_VOTE_AVG = "7.5";
    private static final long MOVIE_ID = 123;
    private static final int POSITION = 0;

    private MoviesModelAdapterImpl modelAdapter;

    @Mock
    Movies movie;

    private List<Movies> moviesList;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Api.class, DateFormatter.class);
        when(Api.getImageBaseUrl()).thenReturn(IMAGE_BASE);
        when(DateFormatter.getReleaseDate(MOVIE_RELEASE_DATE)).thenReturn(FORMATTED_DATE);
        when(movie.getTitle()).thenReturn(MOVIE_TITLE);
        when(movie.getOverview()).thenReturn(MOVIE_OVERVIEW);
        when(movie.getVoteAverage()).thenReturn(MOVIE_VOTE_AVG);
        when(movie.getPosterPath()).thenReturn(IMAGE_BASE + MOVIE_IMAGE_URL);
        when(movie.getReleaseDate()).thenReturn(MOVIE_RELEASE_DATE);
        when(movie.getMovieId()).thenReturn(MOVIE_ID);

        moviesList = new ArrayList<>();
        moviesList.add(movie);

        modelAdapter = new MoviesModelAdapterImpl(moviesList, true, FILTER_DATE);
    }

    @Test
    public void testGetCount() {
        assertEquals(modelAdapter.getCount(), 2);
    }

    @Test
    public void testGetMovieTitle() {
        assertEquals(modelAdapter.getMovieTitle(POSITION), MOVIE_TITLE);
        verify(movie).getTitle();
    }

    @Test
    public void testGetDesc() {
        assertEquals(modelAdapter.getDesc(POSITION), MOVIE_OVERVIEW);
        verify(movie).getOverview();
    }

    @Test
    public void testGetImageUrl() {
        assertEquals(modelAdapter.getImageUrl(POSITION), IMAGE_BASE + MOVIE_IMAGE_URL);
        verify(movie).getPosterPath();
    }

    @Test
    public void testGetAverageVote() {
        assertEquals(modelAdapter.getAverageVote(POSITION), STR_MOVIE_VOTE_AVG);
        verify(movie).getVoteAverage();
    }

    @Test
    public void testGetReleaseDate() {
        assertEquals(modelAdapter.getReleaseDate(POSITION), FORMATTED_DATE);
        verify(movie).getReleaseDate();
    }

    @Test
    public void testGetMovieId() {
        assertEquals(modelAdapter.getMovieId(POSITION), MOVIE_ID);
        verify(movie).getMovieId();
    }
}
