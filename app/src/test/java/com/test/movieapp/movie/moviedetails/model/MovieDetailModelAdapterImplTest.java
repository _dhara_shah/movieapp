package com.test.movieapp.movie.moviedetails.model;

import com.test.movieapp.network.api.Api;
import com.test.movieapp.network.entity.MovieDetailResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@PrepareForTest(Api.class)
@RunWith(PowerMockRunner.class)
public class MovieDetailModelAdapterImplTest {
    private final static String MOVIE_TITLE = "title";
    private final static String MOVIE_OVERVIEW = "overview";
    private final static String MOVIE_BASE_URL = "baseUrl";
    private final static String MOVIE_IMAGE_URL = "imageUrl";

    private MovieDetailModelAdapterImpl modelAdapter;

    @Mock
    MovieDetailResponse response;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Api.class);
        when(Api.getImageBaseUrl()).thenReturn(MOVIE_BASE_URL);
        when(response.getTitle()).thenReturn(MOVIE_TITLE);
        when(response.getOverview()).thenReturn(MOVIE_OVERVIEW);
        when(response.getBackdropPath()).thenReturn(MOVIE_IMAGE_URL);
        modelAdapter = new MovieDetailModelAdapterImpl(response);
    }

    @Test
    public void testGetTitle() {
        assertEquals(modelAdapter.getTitle(), MOVIE_TITLE);
        verify(response).getTitle();
    }

    @Test
    public void testGetOverview() {
        assertEquals(modelAdapter.getOverview(), MOVIE_OVERVIEW);
        verify(response).getOverview();
    }

    @Test
    public void testGetImageUrl() {
        when(response.getPosterPath()).thenReturn(MOVIE_IMAGE_URL);
        assertEquals(modelAdapter.getImageUrl(), MOVIE_BASE_URL + MOVIE_IMAGE_URL);
        verify(response).getPosterPath();
        verify(response).getBackdropPath();
    }

    @After
    public void testAfter() {
        verifyNoMoreInteractions(response);
    }
}
