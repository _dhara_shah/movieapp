package com.test.movieapp.movie.home.presenter;

import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;

import com.test.movieapp.MovieAppBaseTest;
import com.test.movieapp.movie.home.model.MainInteractor;
import com.test.movieapp.movie.home.model.MoviesModelAdapter;
import com.test.movieapp.movie.home.router.MainRouter;
import com.test.movieapp.movie.home.view.MainView;
import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.entity.MovieResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class MainPresenterImplTest extends MovieAppBaseTest {
    private final static long MOVIE_ID = 1234;
    private final static String CURRENT_DATE = "2017-12-27";
    private MainPresenterImpl presenter;

    @Mock
    MainView view;
    @Mock
    MainRouter router;
    @Mock
    MainInteractor interactor;
    @Mock
    MovieResponse movieResponse;
    @Captor
    ArgumentCaptor<MainInteractor.ResponseListener> responseListener;
    @Captor
    ArgumentCaptor<MoviesModelAdapter> modelAdapter;
    @Mock
    View layoutView;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        Mockito.when(movieAppModule.provideMainInteractor()).thenReturn(interactor);
        presenter = new MainPresenterImpl(view, router);
    }

    @Test
    public void testHandleOnViewCreated() {
        presenter.handleOnViewCreated();
        verify(view).setInteractionListener(eq(presenter));
        verify(view).initViews(modelAdapter.capture());
        verify(interactor).getMovieList();
        verify(interactor).isLoadMore();
        verify(interactor).getFilterDate();
        verify(view).showProgress();
        verify(interactor).fetchMovies(responseListener.capture(), eq(true));
    }

    @Test
    public void testOnResponseSuccess() {
        presenter.onResponseSuccess(movieResponse);
        verify(view).hideProgress();
        verify(view).updateMovies(modelAdapter.capture());
        verify(interactor).getMovieList();
        verify(interactor).isLoadMore();
        verify(interactor).getFilterDate();
    }

    @Test
    public void testOnResponseError() {
        final ServiceError error = mock(ServiceError.class);
        when(error.getErrorMessage()).thenReturn("error");
        presenter.onResponseError(error);
        verify(view).hideProgress();
        verify(view).showError(eq("error"));
    }

    @Test
    public void testOnMoreInfoClicked() {
        final ActivityOptionsCompat options = mock(ActivityOptionsCompat.class);
        when(view.getActivityOptions(layoutView)).thenReturn(options);
        presenter.onMoreInfoClicked(layoutView, MOVIE_ID);
        verify(view).getActivityOptions(layoutView);
        verify(router).openDetails(eq(MOVIE_ID), eq(options));
    }

    @Test
    public void testOnLoadMoreTrue() {
        when(interactor.isLoadMore()).thenReturn(true);
        presenter.onLoadMore();
        verify(interactor).isLoadMore();
        verify(interactor).fetchMovies(responseListener.capture(), eq(false));
    }

    @Test
    public void testOnLoadMoreFalse() {
        when(interactor.isLoadMore()).thenReturn(false);
        presenter.onLoadMore();
        verify(interactor).isLoadMore();
        verify(view).endLoadMore();
    }

    @Test
    public void testOnFilterClicked() {
        presenter.handleOnFilterClicked();
        verify(view).showDatePicker();
    }

    @Test
    public void testOnDateSet() {
        presenter.onDateSet(CURRENT_DATE);
        verify(interactor).setFilterDate(eq(CURRENT_DATE));
        verify(view).showProgress();
        verify(interactor).fetchMovies(responseListener.capture(), eq(true));
    }

    @After
    public void testAfter() {
        verifyNoMoreInteractions(interactor, view, movieResponse, router);
    }
}
