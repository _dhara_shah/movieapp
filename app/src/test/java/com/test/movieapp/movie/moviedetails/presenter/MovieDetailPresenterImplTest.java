package com.test.movieapp.movie.moviedetails.presenter;

import com.test.movieapp.MovieAppBaseTest;
import com.test.movieapp.movie.moviedetails.model.MovieDetailInteractor;
import com.test.movieapp.movie.moviedetails.model.MovieDetailModelAdapter;
import com.test.movieapp.movie.moviedetails.router.MovieDetailRouter;
import com.test.movieapp.movie.moviedetails.view.MovieDetailView;
import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.entity.MovieDetailResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(PowerMockRunner.class)
public class MovieDetailPresenterImplTest extends MovieAppBaseTest {
    private MovieDetailPresenterImpl presenter;
    @Mock
    MovieDetailView view;
    @Mock
    MovieDetailInteractor interactor;
    @Mock
    MovieDetailRouter router;
    @Mock
    MovieDetailResponse response;
    @Mock
    ServiceError error;
    @Captor
    ArgumentCaptor<MovieDetailModelAdapter> modelAdapter;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        presenter = new MovieDetailPresenterImpl(view, interactor, router);
    }

    @Test
    public void testOnViewCreated() {
        presenter.onViewCreated();
        verify(view).initViews(eq(presenter));
        verify(view).showProgress();
        verify(interactor).fetchDetail(eq(presenter));
    }

    @Test
    public void testHandleOnBackPress() {
        presenter.handleOnBackPress();
        verify(router).close();
    }

    @Test
    public void testOnResponseSuccess() {
        presenter.onResponseSuccess(response);
        verify(view).hideProgress();
        verify(view).updateData(modelAdapter.capture());
    }

    @Test
    public void testOnResponseError() {
        presenter.onResponseError(error);
        verify(view).hideProgress();
    }

    @After
    public void testAfter() {
        verifyNoMoreInteractions(interactor, router, view, response, error);
    }
}
