# MovieApp

1. This application uses TMDB API to list the movies, and display the details of each movie. 
2. Supports both the landscape and portrait mode, with toggle between list and grid view
3. Does not support storing of movies into the database
4. Filtering of movies is based on the release date, so any movie that has a release date before or equal to the selected date will be displayed
5. Sorting internally happens based on the release date descending (via API)
6. Support unit testing, espresso testing
7. Follows the MVP pattern with Interactors and Routers.

# Libraries used
1. Retrofit
2. Gson converter , Ok HTTP
3. Support libraries, Design, constraintLayout, RecyclerView
4. Unit testing 
5. Mockito and Powermockito
6. Espresso related
7. Iconics library to support iconfont in the app where we have icons
8. Iconics fontawesome library
9. Dagger2

# Improvements that can be made
1. UI improvements
2. Storing of data
3. More filters can be added
4. Search etc.
5. The detail screen currently uses the backdrop image which does not exist for some movies, we can have placeholders for them or else use the poster image but improve on the layout

# APKs

The debug and androidTest apks are attached to this source code

# Credits (UI Inspiration)
https://www.uplabs.com/posts/food-app-animation
